import java.util.Scanner;
import java.util.*;
public class Game {

    private static final String SOLVED_ID = "123456780";
    Board theBoard;
    String originalBoardID;
    String boardName;

    class Node<T> {
        T data;
        Node<T> next;

        public Node(T data) {
            this.data = data;
            this.next = null;
        }
    }
    class LinkedList<T> {
        private Node<T> head;

        public LinkedList() {
            this.head = null;
        }
        public void add(T data) {
            Node<T> newNode = new Node<T>(data);
            if (head == null) {
                head = newNode;
            } else {
                Node<T> current = head;
                while (current.next!= null) {
                    current = current.next;
                }
                current.next = newNode;
            }
        }
        public T remove() {
            if (head == null) {
                return null;
            }
            T data = head.data;
            head = head.next;
            return data;
        }

        public boolean isEmpty() {
            return head == null;

        }


        public void printContents() {
            Node<T> current = head;
            while (current!= null) {
                System.out.println(current.data);
                current = current.next;
            }
            System.out.println();
        }
    }


    /**
     * Solve the provided board
     * @param label Name of board (for printing)
     * @param b Board to be solved
     */


    public void playGiven(String label, Board b) {
        theBoard = b;
        originalBoardID = b.getId();
        boardName = label;
        System.out.println("Board initial: " + boardName + " \n" +
                theBoard.toString());
        solve();
    }

    public void solve() {
        LinkedList<State> queue = new LinkedList<>();
        boolean[][] visited = new boolean[3][3];
        State initialState = new State(originalBoardID, "");
        queue.add(initialState);

        while (!queue.isEmpty()) {
            State currentState = queue.remove(); // Remove the front of the queue

            if (currentState.getId().equals(SOLVED_ID)) {
                System.out.println("Solution steps for " + boardName + ": " + currentState.getSteps());
                return;
            }

            int blankRow = theBoard.getBlankRow();
            int blankCol = theBoard.getBlankCol();

            if (blankRow >= 0 && blankRow < 3 && blankCol >= 0 && blankCol < 3) {
                // Try moving up
                if (blankRow > 0 && !visited[blankRow - 1][blankCol]) {
                    if (theBoard.slideUp()) {
                        String newSteps = currentState.getSteps() + "U";
                        queue.add(new State(theBoard.getId(), newSteps));
                        theBoard.slideDown(); // Undo the move
                    }
                }
                // Try moving down
                if (blankRow < 2 && !visited[blankRow + 1][blankCol]) {
                    if (theBoard.slideDown()) {
                        String newSteps = currentState.getSteps() + "D";
                        queue.add(new State(theBoard.getId(), newSteps));
                        theBoard.slideUp(); // Undo the move
                    }
                }
                // Try moving left
                if (blankCol > 0 && !visited[blankRow][blankCol - 1]) {
                    if (theBoard.slideLeft()) {
                        String newSteps = currentState.getSteps() + "L";
                        queue.add(new State(theBoard.getId(), newSteps));
                        theBoard.slideRight(); // Undo the move
                    }
                }
                // Try moving right
                if (blankCol < 2 && !visited[blankRow][blankCol + 1]) {
                    if (theBoard.slideRight()) {
                        String newSteps = currentState.getSteps() + "R";
                        queue.add(new State(theBoard.getId(), newSteps));
                        theBoard.slideLeft(); // Undo the move
                    }
                }
                visited[blankRow][blankCol] = true;
            }
        }
        System.out.println("No solution found for " + boardName);
    }

    /**
         * Create a random board (which is solvable) by jumbling jumnbleCount times.
         * Solve
         * @param label Name of board (for printing)
         * @param jumbleCount number of random moves to make in creating a board
         */
    public void playRandom(String label, int jumbleCount) {
        theBoard = new Board();
        theBoard.makeBoard(jumbleCount);
        System.out.println(label + "\n" + theBoard);
        playGiven(label, theBoard);
    }
    public static void main(String[] args) {
        String[] games = {"102453786", "123740658", "023156478", "413728065",
                "145236078", "123456870"};
        String[] gameNames = {"Easy Board", "Game1", "Game2", "Game3", "Game4",
                "Game5 No Solution"};
        Game g = new Game();
        Scanner in = new Scanner(System.in);
        Board b;
        String resp;
        for (int i = 0; i < games.length - 1; i++) {
            b = new Board(games[i]);
            g.playGiven(gameNames[i], b);
            System.out.println("Click any key to continue\n");
            resp = in.nextLine();
        }
        boolean playAgain = true;
//playAgain = false;
        int JUMBLECT = 18; // how much jumbling to do in random board
        while (playAgain) {
            g.playRandom("Random Board", JUMBLECT);
            System.out.println("Play Again? Answer Y for yes\n");
            resp = in.nextLine().toUpperCase();
            playAgain = (resp != "") && (resp.charAt(0) == 'Y');
        }
    }
}